package com.libertymutual.student.beth.programs.example01.shapes;

import org.junit.Test;

import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class SquareTest {

    @Test
    public void testGetArea() {
        Square square = new Square(5, Color.pink);
        BigDecimal area = square.getArea();
        BigDecimal expectedAnswer = new BigDecimal(25);
        assertEquals("Verify that the area is correct", expectedAnswer, area);
    }
    
    @Test
    public void testColor() {
        Square square = new Square(5, Color.pink);
        Color color = square.getColor();
        assertEquals("Verify that the color is correct", Color.pink, color);
    }
}
