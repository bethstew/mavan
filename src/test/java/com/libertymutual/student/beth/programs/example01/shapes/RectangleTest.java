package com.libertymutual.student.beth.programs.example01.shapes;

import org.junit.Test;

import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class RectangleTest {

    @Test
    public void testGetArea() {
        Rectangle rectangle = new Rectangle(10, 5, Color.blue);
        BigDecimal area = rectangle.getArea();
        BigDecimal expectedAnswer = new BigDecimal(50);
        assertEquals("Verify that the area is correct", expectedAnswer, area);
    }
    
    @Test
    public void testColor() {
        Rectangle rectangle = new Rectangle(10, 5, Color.blue);
        Color color = rectangle.getColor();
        assertEquals("Verify that the color is correct", Color.blue, color);
    }
}
