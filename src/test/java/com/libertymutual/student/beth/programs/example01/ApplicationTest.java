package com.libertymutual.student.beth.programs.example01;

import org.junit.Test;
import org.junit.Rule;
import static org.junit.Assert.assertEquals;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

public class ApplicationTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();
    
	@Test
    public void testMain() {
       String[] args = new String[0];
	   Application.main(args);
    }
}
