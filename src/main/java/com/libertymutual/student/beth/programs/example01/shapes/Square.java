package com.libertymutual.student.beth.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Square extends Shape {

		private final static Logger logger = LogManager.getLogger(Square.class);

		private int length;

		public Square (int length, Color color) {
				super(color);
				this.length = length;
		}

		// provide a getArea implementation
    	@Override
		public BigDecimal getArea() {
				double area = length * length;
				return new BigDecimal(area);
		}
}

