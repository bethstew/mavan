package com.libertymutual.student.beth.programs.example01;

import java.awt.Color;
import java.math.BigDecimal;
import com.libertymutual.student.beth.programs.example01.shapes.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
	
	private final static Logger logger = LogManager.getLogger(Application.class);

	public static void main(String[] args) {
		
		logger.info(Application.class + " starting application now ");

		int radius = 25;
		Circle circle = new Circle(radius, Color.PINK);
		System.out.println(circle.getArea());
		System.out.println(circle.getColor());

		int length = 100;
		Square square = new Square(length, Color.RED);
		System.out.println(square.getArea());
		System.out.println(square.getColor());
		
		int width = 50;
		Rectangle rectangle = new Rectangle(length, width, Color.BLUE);
		System.out.println(rectangle.getArea());
		System.out.println(rectangle.getColor());

		Application application = new Application();
		//System.exit(0);
	}
}

